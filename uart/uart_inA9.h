#ifndef _INITM0_H
#define _INITM0_H
    
    enum M0_ERR_TYPE
    {
        M0_ERR = -1,
        MO_OK
    };
    int ininM0(void);
    void M0_recv(int , unsigned char *);
    void M0_command(int , unsigned char *);
    int set_serial(int MOfd,int nSpeed,int nBits,char nEvent,int nStop);

#endif