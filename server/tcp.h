/*************************************************************************
	> File Name: tcp.h
	> Author: 
	> Mail: 
	> Created Time: Fri 01 Mar 2019 11:18:54 AM CST
 ************************************************************************/

#ifndef _TCP_H
#define _TCP_H
#include<stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <arpa/inet.h>

#define MAX_BACKLOG 1024

int tcp_server_init(const char *ip, const char *port);
int tcp_server_wait_connect(int listenfd);
ssize_t tcp_server_recv(int connfd, void *buf, size_t count);
ssize_t tcp_server_send(int connfd, const void *buf, size_t count);

struct data
{
	int head;
	char user[20];
	char pwd[20];
};


#endif
