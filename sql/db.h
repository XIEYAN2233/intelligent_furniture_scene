/*************************************************************************
	> File Name: db.h
	> Author: 
	> Mail: 
	> Created Time: 2019年02月28日 星期四 11时13分25秒
 ************************************************************************/

#ifndef _DB_H
#define _DB_H
#include "sqlite3.h"
#include <sys/stat.h>
#include <fcntl.h>

enum DB_ERROR_TYPE
{
    DB_insyock_ok = 1,
    in_out_ok = 2,
    DB_ERROR_OK,
    DB_ERROR_FAILURE,
    DB_ERROR_INVALID_PARAM
};
int Mysqlite_in_oper(char *ptime,char *ptem char *phyg,char *psmog);
long InitConnections(void);
long CloseConnections(sqlite3 *pDB);
int Mysqlite_out_oper(char *ptime,char *ptem,char *phyg,char *psmog);
int Mysqlite_select_infor(char* SCid);

extern sqlite3 *g_db;

#endif
